
module SampleGem

  class GemRunner
    def initialize(number)
      @number = number
    end

   def facto
      fact = 1;
      (1..@number).each do |i|
      fact = fact*i;
      end
      print "Factorial of ", @number ," is: ", fact
      fact
      end

   def prime
     count=0
     num = @number
     if (num==0)
     	puts "0 is not prime"
     else

     	i=2
     	while(i<num)
     		if (num%i==0)
     			count+=1
     		end
     		i+=1
     	end

     end
     	if count>1
     		puts "#{num} is not a prime number"
        false
     	else
     		puts "#{num} is a prime number"
        true
    	end

   end

   def sqrt
     num = @number
     res = Integer.sqrt(num)
     res
   end

    end
end

require_relative 'lib/sample_gem/version'

Gem::Specification.new do |spec|
  spec.name          = "sampleGem"
  spec.version       = SampleGem::VERSION
  spec.authors       = ["shrikant.shelar"]
  spec.email         = ["shrikant.shelar@globant.com"]

  spec.summary       = %q{Write a short summary, because RubyGems requires one.}
  spec.description   = %q{Write a longer description or delete this line.}
  spec.homepage      = 'https://rubygems.org/gems/sampleGem'
  spec.license       = "MIT"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.

  spec.files         = ["lib/sample_gem.rb","lib/sample_gem/gem_runner.rb", "lib/sample_gem/version.rb"]
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end

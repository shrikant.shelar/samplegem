require "./lib/sample_gem/gem_runner"

RSpec.describe SampleGem::GemRunner do

  it "finds the factorial of number ::" do
    o = SampleGem::GemRunner.new 3
#stub
   allow(o).to receive(:facto).and_return(6)
#mock
     expect( o.facto ).to eq(6)
  end



  it "finds the prime of number ::" do
    o = SampleGem::GemRunner.new 3
#stub
   allow(o).to receive(:prime).and_return(true)
#mock
     expect( o.prime ).to eq(true)
  end


  it "finds the sqrt of number ::" do
    o = SampleGem::GemRunner.new 3
#stub
   allow(o).to receive(:sqrt).and_return(2)
#mock
     expect( o.sqrt ).to eq(2)
  end


end
